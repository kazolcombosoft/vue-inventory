<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\Pos;
use App\Model\Extra;
use App\Model\Order;
use App\Model\OrderProduct;
use DB;

class PosController extends Controller
{
    public function categoryProduct($catId){
        $categoryProduct = Product::where('category_id',$catId)->get();
        return response()->json($categoryProduct);
    }

    public function addToCart(Request $request, $pId){
        $product = Product::find($pId);
        $posProduct = Pos::where('pro_id',$pId)->first();
        if($posProduct){
            $posProduct->increment('pro_quantity');
            $this->updateSubTotal($posProduct->id);
        }else{
            $pos = new Pos();
            $pos->pro_id = $pId;
            $pos->pro_name = $product->product_name;
            $pos->pro_quantity = 1;
            $pos->pro_price = $product->selling_price;
            $pos->sub_total = $product->selling_price;
            $pos->save();
            return response()->json($pos);
        }

    }

    public function cartProduct(){
        $cartProducts = Pos::all();
        return response()->json($cartProducts);
    }

    public function removeItemFromCart($cartId){
        $cartRemove = Pos::destroy($cartId);
        return response()->json($cartRemove);
    }

    public function incrementCart($id){
        $posProduct = Pos::find($id)->increment('pro_quantity');
        $this->updateSubTotal($id);
    }

    public function decrementCart($id){
        $posProduct = Pos::find($id)->decrement('pro_quantity',1);
        $this->updateSubTotal($id);
    }

    public function updateSubTotal($id){
        $afterIncrement = Pos::find($id);
        $subTotal = $afterIncrement->pro_quantity * $afterIncrement->pro_price;
        $afterIncrement->update(['sub_total' => $subTotal]);
    }

    public function getVat(){
        $vat = Extra::first();
        return response()->json($vat);
    }

    public function placeOrder(Request $request){
        $validateData = $request->validate([
            'customer_id' => 'required',
            'pay_by' => 'required'
        ]);

        $orderInfo = new Order();
        $orderInfo->customer_id = $request->customer_id;
        $orderInfo->qty = $request->qty;
        $orderInfo->sub_total = $request->sub_total;
        $orderInfo->vat = $request->vat;
        $orderInfo->total = $request->total;
        $orderInfo->pay = $request->pay;
        $orderInfo->due = $request->due;
        $orderInfo->pay_by = $request->pay_by;
        $orderInfo->order_date = date('d/m/Y');
        $orderInfo->order_month = date('F');
        $orderInfo->order_year = date('Y');
        $orderInfo->save();

        $cartProduct = Pos::all();
        foreach($cartProduct as $product){
            $orderProduct = new OrderProduct();
            $orderProduct->order_id = $orderInfo->id;
            $orderProduct->product_id = $product->pro_id;
            $orderProduct->pro_quantity = $product->pro_quantity;
            $orderProduct->product_price = $product->pro_price;
            $orderProduct->sub_total = $product->sub_total;
            $orderProduct->save();

            $decrementInventory = Product::find($product->pro_id)->decrement('product_quantity',$product->pro_quantity);

        }
        DB::table('pos')->delete();
        return response()->json('done');

    }


}
