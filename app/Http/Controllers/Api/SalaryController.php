<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Salary;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function paid(Request $request, $id){
        $validateData = $request->validate([
            'salary_month' => 'required'
        ]);
        $check = Salary::where('employee_id',$id)->where('salary_month',$request->salary_month)->first();
        if($check){
            return response()->json('Already Paid Salary');
        }else{
            $salary = new Salary();
            $salary->employee_id = $id;
            $salary->amount = $request->salary;
            $salary->salary_date = date('d/m/Y');
            $salary->salary_month = $request->salary_month;
            $salary->salary_year = date('Y');
            $salary->save();
        }
    }

    public function allSalaryMonth(){
        $salaryMonth = Salary::select('salary_month')->groupBy('salary_month')->get();
        return response()->json($salaryMonth);
    }

    public function salaryMonthWise($month){
        $employeeSalary = Salary::join('employees','salaries.employee_id','=','employees.id')
        ->where('salaries.salary_month',$month)
        ->select('employees.name','salaries.*')
        ->get();
        return response()->json($employeeSalary);
    }

    public function editSalary($salary_id){
        $employeeSalary = Salary::join('employees','salaries.employee_id','=','employees.id')
        ->where('salaries.id',$salary_id)
        ->select('employees.name','employees.email','salaries.*')
        ->first();
        return response()->json($employeeSalary);
    }

    public function updateSalary(Request $request,$salary_id){
        $empInfo = Salary::find($salary_id);
        $empInfo->amount = $request->amount;
        $empInfo->salary_month = $request->salary_month;
        $empInfo->employee_id = $request->employee_id;
        $empInfo->save();
    }



}
