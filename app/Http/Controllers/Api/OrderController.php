<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DateTime;
use App\Model\Order;
use App\Model\Expense;

class OrderController extends Controller
{
   public function allOrder(){
    $allOrder = DB::table('orders')
    ->join('customers','orders.customer_id','=','customers.id')
    ->where('orders.order_date',date('d/m/Y'))
    ->select('orders.*','customers.name')
    ->get();
    return response()->json($allOrder);
   }

   public function viewOrder($id){
      $orderInfo = DB::table('orders')
      ->join('customers','orders.customer_id','=','customers.id')
      ->where('orders.id',$id)
      ->select('customers.name','customers.phone','customers.address','orders.*')
      ->first();
      return response()->json($orderInfo);
   }

   public function viewAllOrder($id){
      $orderInfo = DB::table('order_products')
      ->join('products','order_products.product_id','=','products.id')
      ->where('order_products.order_id',$id)
      ->select('products.product_name','products.product_code','products.image','order_products.*')
      ->get();
      return response()->json($orderInfo);
   }

   public function searchOrder(Request $request){
      $date = new DateTime($request->date);
      $formateDate = $date->format('d/m/Y');
      $orderInfo = DB::table('orders')
      ->join('customers','orders.customer_id','=','customers.id')
      ->where('orders.order_date',$formateDate)
      ->select('customers.name','orders.*')
      ->get();
      return response()->json($orderInfo);
   }

   public function todayOrder(){
      $date = date('d/m/Y');
      $todaySell = Order::where('order_date',$date)->sum('total');
      return response()->json($todaySell);
   }

   public function income(){
      $date = date('d/m/Y');
      $income = Order::where('order_date',$date)->sum('pay');
      return response()->json($income);
   }

   public function due(){
      $date = date('d/m/Y');
      $due = Order::where('order_date',$date)->sum('due');
      return response()->json($due);
   }

   public function expense(){
      $date = date('Y-m-d');
      $expense = Expense::where('expense_date',$date)->sum('amount');
      return response()->json($expense);
   }


}
