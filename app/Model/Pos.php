<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pos extends Model
{
    protected $table = 'pos';
    protected $fillable = [
        'pro_id', 'pro_name', 'pro_quanity', 'pro_price','sub_total'
    ];
}
