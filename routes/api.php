<?php

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signUp');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::apiResource('employee','Api\EmployeeController');
Route::apiResource('supplier','Api\SupplierController');
Route::apiResource('category','Api\CategoryController');
Route::apiResource('product','Api\ProductController');
Route::apiResource('customer','Api\CustomerController');
Route::apiResource('expense','Api\ExpenseController');
Route::post('salary/paid/{id}','Api\SalaryController@paid');
Route::get('salary/','Api\SalaryController@allSalaryMonth');
Route::get('salary/view/{month}','Api\SalaryController@salaryMonthWise');
Route::get('salary/edit/{salary_id}','Api\SalaryController@editSalary');
Route::post('salary/update/{salary_id}','Api\SalaryController@updateSalary');
Route::post('stock/update/{id}','Api\ProductController@updateStock');
Route::post('stock/update/{id}','Api\ProductController@updateStock');
Route::get('category-product/{cat_id}','Api\PosController@categoryProduct');

//POS rotues start
Route::get('pos/add-to-cart/{producdt_id}','Api\PosController@addToCart');
Route::get('pos/cart-product','Api\PosController@cartProduct');
Route::get('pos/remove-item-from-cart/{cartId}','Api\PosController@removeItemFromCart');
Route::get('pos/increment/{id}','Api\PosController@incrementCart');
Route::get('pos/decrement/{id}','Api\PosController@decrementCart');
Route::get('pos/vat','Api\PosController@getVat');

Route::post('pos/place-order','Api\PosController@placeOrder');
//POS routes end

//Order routes start
Route::get('pos/order','Api\OrderController@allOrder');
Route::get('pos/view-order/{id}','Api\OrderController@viewOrder');
Route::get('pos/view-all-order/{id}','Api\OrderController@viewAllOrder');
Route::post('pos/search-order','Api\OrderController@searchOrder');
Route::get('pos/today-sell','Api\OrderController@todayOrder');
Route::get('pos/income','Api\OrderController@income');
Route::get('pos/due','Api\OrderController@due');
Route::get('pos/expense','Api\OrderController@expense');
Route::get('pos/out-of-stock','Api\ProductController@outOfStock');
//Order routes end



// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
