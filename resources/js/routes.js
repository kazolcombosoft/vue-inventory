//Auth component start
import Login from './components/auth/Login'
import Register from './components/auth/Register'
import ForgetPassword from './components/auth/ForgetPassword'
import Logout from './components/auth/Logout'
import Home from './components/Home'
//Auth component end

//Employee component start
import AddEmployee from './components/employee/AddEmployee'
import AllEmployee from './components/employee/AllEmployee'
import EditEmployee from './components/employee/EditEmployee'
//Employee component end

//Supplier component start
import AddSupplier from './components/supplier/AddSupplier'
import AllSupplier from './components/supplier/AllSupplier'
import EditSupplier from './components/supplier/EditSupplier'
//Supplier component end

//Category component start
import AddCategory from './components/category/AddCategory'
import AllCategory from './components/category/AllCategory'
import EditCategory from './components/category/EditCategory'
//Category component end

//Product component start
import AddProduct from './components/product/AddProduct'
import AllProduct from './components/product/AllProduct'
import EditProduct from './components/product/EditProduct'
//Product component end

//Customer component start
import AddCustomer from './components/customer/AddCustomer'
import AllCustomer from './components/customer/AllCustomer'
import EditCustomer from './components/customer/EditCustomer'
//Customer component end

//Expense component start
import AddExpense from './components/expense/AddExpense'
import AllExpense from './components/expense/AllExpense'
import EditExpense from './components/expense/EditExpense'
//Expense component end

//Salary component start
import GivenSalary from './components/salary/GivenSalary'
import PaySalary from './components/salary/PaySalary'
import AllSalary from './components/salary/AllSalary'
import ViewSalary from './components/salary/ViewSalary'
import EditSalary from './components/salary/EditSalary'
//Salary component end

//Stock component start
import Stock from './components/product/Stock'
import EditStock from './components/product/EditStock'
//Stock component end

//POS component start
import POS from './components/pos/PointOfSale'
//POS component end

//Order component start
import Order from './components/order/Order'
import ViewOrder from './components/order/ViewOrder'
import SearchOrder from './components/order/SearchOrder'
//Order component end

export const routes = [
    //Auth routes start
    { path: '/', name: '/', component: Login },
    { path: '/register', name: 'Register', component: Register },
    { path: '/forget-password', name: 'ForgetPassword', component: ForgetPassword },
    { path: '/logout', name: 'Logout', component: Logout },
    //Auth routes end

    { path: '/home', name: 'Home', component: Home },

    //Employee routes start
    {path: '/add-employee', name: 'AddEmployee', component: AddEmployee},
    {path: '/all-employee', name: 'AllEmployee', component: AllEmployee},
    {path: '/edit-employee/:id', name: 'EditEmployee', component: EditEmployee},
    //Employee routes end

    //Supplier routes start
    {path: '/add-supplier', name: 'AddSupplier', component: AddSupplier},
    {path: '/all-supplier', name: 'AllSupplier', component: AllSupplier},
    {path: '/edit-supplier/:id', name: 'EditSupplier', component: EditSupplier},
    //Supplier routes end

    //Category routes start
    {path: '/add-category', name: 'AddCategory', component: AddCategory},
    {path: '/all-category', name: 'AllCategory', component: AllCategory},
    {path: '/edit-category/:id', name: 'EditCategory', component: EditCategory},
    //Category routes end

    //Product routes start
    {path: '/add-product', name: 'AddProduct', component: AddProduct},
    {path: '/all-product', name: 'AllProduct', component: AllProduct},
    {path: '/edit-product/:id', name: 'EditProduct', component: EditProduct},
    //Product routes end

    //Customer routes start
    {path: '/add-customer', name: 'AddCustomer', component: AddCustomer},
    {path: '/all-customer', name: 'AllCustomer', component: AllCustomer},
    {path: '/edit-customer/:id', name: 'EditCustomer', component: EditCustomer},
    //Customer routes end

    //Expense routes start
    {path: '/add-expense', name: 'AddExpense', component: AddExpense},
    {path: '/all-expense', name: 'AllExpense', component: AllExpense},
    {path: '/edit-expense/:id', name: 'EditExpense', component: EditExpense},
    //Expense routes end

    //Salary routes start
    {path: '/given-salary', name: 'GivenSalary', component: GivenSalary},
    {path: '/pay-salary/:id', name: 'PaySalary', component: PaySalary},
    {path: '/salary', name: 'AllSalary', component: AllSalary},
    {path: '/salary/view/:month', name: 'ViewSalary', component: ViewSalary},
    {path: '/salary/edit/:id', name: 'EditSalary', component: EditSalary},
    //Salary routes end

    //Stock routes start
    {path: '/stock', name: 'Stock', component: Stock},
    {path: '/edit-stock/:id', name: 'EditStock', component: EditStock},
    //Stock routes end

    //POS routes start
    {path: '/pos', name: 'pos', component: POS},
    //POS routes end

    //Order routes start
    {path: '/order', name: 'Order', component: Order},
    {path: '/view-order/:id', name: 'ViewOrder', component: ViewOrder},
    {path: '/search-order', name: 'SearchOrder', component: SearchOrder},
    //Order routes end
  ]