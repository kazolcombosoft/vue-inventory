class Notification{
    success(){
        new Noty({
            type: 'success',
            text: 'Successfull',
            layout: 'topRight',
            timeout: 3000
        }).show();
    }
    alert(){
        new Noty({
            type: 'alert',
            text: 'Are you sure?',
            layout: 'topRight',
            timeout: 3000
        }).show();
    }
    warning(){
        new Noty({
            type: 'warning',
            text: 'Something Went Wrong',
            layout: 'topRight',
            timeout: 3000
        }).show();
    }
    error(){
        new Noty({
            type: 'error',
            text: 'Oops Went Wrong',
            layout: 'topRight',
            timeout: 3000
        }).show();
    }
    imageValidationMessage(){
        new Noty({
            type: 'error',
            text: 'Upload image must be less than 1MB',
            layout: 'topRight',
            timeout: 3000
        }).show();
    }
    cart_succes_msg(){
        new Noty({
            type: 'success',
            text: 'Add To Cart Successfully',
            layout: 'topRight',
            timeout: 3000
        }).show();
    }
    cart_remove_msg(){
        new Noty({
            type: 'success',
            text: 'Remove Item From Cart',
            layout: 'topRight',
            timeout: 3000
        }).show();
    }
}
export default Notification = new Notification()